<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addition extends Model
{
    protected $guarded = ['id'];

    public function product(){
        return $this->belongsToMany(Product::class);
    }


}

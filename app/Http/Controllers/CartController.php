<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartItems= \Cart::getContent();
//        dd($cartItems);

//     return response()->json(['status'=>1,'message'=>'success','item'=>$cartItems]);

        return view('carts',compact('cartItems'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     //dd(555);
     \Cart::remove($id);
      return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,$id)
    {
//        dd($request->all());
//        dd($id);
        \Cart::update($id, array(
            'quantity' => array(
                'relative' => false,
                'value' => $request->quantity,
            ),
        ));
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return response()->json(['aaaaaa'=>333345,'message'=>'success']);
//      dd(66666);
        $Product = Product::find($id);
//        dd($Product->id);
        \Cart::add($id, $Product->name,$Product->price,$request->quantity,['image'=>$Product->image]);
//        dd( \Cart::getContent()) ;
        return redirect()->route('cart.index',compact('Product'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function confirm()
    {
//        $order = Order::all();
//        dd($order);


        $cartItems = \Cart::getContent();
        $ids=[];
        $sum=0;

        foreach($cartItems as $cartItem){
            $ids[] = (int)$cartItem->id;
//            dd($ids);
            $sum=$sum+ $cartItem->getPriceSum();
        }
//        dd($ids);
//      return $sum;
        $total=$sum;
        return view('completerequest',compact('cartItems','total'));
    }


}

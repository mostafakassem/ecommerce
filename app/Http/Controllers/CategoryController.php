<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function childs( $id ){

        $cats  = Category::where("parent_id",$id )->select("id","category_name")->get();
        $html="";
        foreach ($cats as $cat){
            $html.= "<option value='".$cat->id."'>".$cat->category_name."</option>" ;
        }
        return response()->json(["status"=>true , 'data'=>$html]) ;


//        html.=  => تربط(تجمع) الابشنات كلها مع بعض

    }

    public function index()
    {
        $categories = Category::where('parent_id',0)->get();
        return view('categories',compact('categories'));
    }


    public function sub($id)
    {
//        dd($id);
        $cats = Category::where('id',$id)->get();
//         return $cats;
        return view('subcat',compact('cats'));
    }
    public function prod($id){
//        dd($id);
        $cats = Category::where('parent_id',$id)->with('product')->get();
//        return $cats;
        return view('prod',compact('cats'));
    }



    //show function because
    public function showproduct($id){
//        dd(55);
        $model = Product::findOrFail($id);
        return view('show',compact('model'));
    }


}

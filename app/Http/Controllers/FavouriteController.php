<?php

namespace App\Http\Controllers;

use App\Favourite;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavouriteController extends Controller
{

//    public function add($product)
//    {
////        return response()->json(['xxxxxxx' => 111111111111111, 'message' => 'success']);
//
//        $user = Auth::user();
//
//        $isFavourite = $user->favourite_products()->where('product_id', $product)->count();
//        if ($isFavourite == 0) {
//            $user->favourite_products()->attach($product);
//            return redirect()->back();
//        } else {
//
//            $user->favourite_products()->detach($product);
//            return redirect()->back();
//        }
//    }

    public function add2($product)
    {
//        return response()->json(['aaaaaa'=>111111111111111,'message'=>'success']);

//        dd($users);
        $user = Auth::user();
        $users = Favourite::where('product_id',$product)->count();
        $isFavourite = $user->favourite_products()->where('product_id', $product)->count();
//        dd($isFavourite);
        if ($isFavourite == 0) {
            $user->favourite_products()->attach($product);
            $users++;
            return response()->json(['status' => 1, 'message' => 'success', 'item' => $isFavourite, 'id' => $product,'p' => $users]);
        } else {
            $user->favourite_products()->detach($product);
            $users--;
            return response()->json(['status' => 0, 'message' => 'success', 'item' => $isFavourite, 'id' => $product,'p' => $users]);
        }
    }
}
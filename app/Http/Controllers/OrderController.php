<?php

namespace App\Http\Controllers;

use App\Favourite;
use App\Order;
use App\OrderProduct;
use App\Product;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    public function index()
    {
        $orders = Order::all();
        return view('orders',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $items =[
            'payment_way' => 'required',
        ];
        $message =[
            'payment_way.required'=>' طريقه الدفع مطلوب',
        ];
        $cartItems = \Cart::getContent();
//        dd($cartItems);
        $total=0;
        $add=(5/100);
        foreach ($cartItems as $cartItem)
        {
            $total = $total + $cartItem->quantity * $cartItem->price;
        }
        $add = $add * $total;
        $net = $total + $add;


        $this->validate($request,$items,$message);
        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->add_value =$add;
        $order->total =$total;
        $order->net =$net;
        $order->payment_way = $request->payment_way;
        $order->save();
//        dd($order->id);
//        dd($order);

        ## create details order in loop

        foreach ($cartItems as $cartItem) {

            $orderDetail = new OrderProduct();
            $orderDetail->product_id = $cartItem->id;
            $orderDetail->order_id = $order->id;
            $orderDetail->price = $cartItem->quantity*$cartItem->price;
            $orderDetail->discount = 0;
            $orderDetail->after_discount = $cartItem->quantity*$cartItem->price;
            $orderDetail->save();
        }
        return redirect()->route('user.index');

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $order = Order::findOrFail($id);
        return view('orderdetails',compact('order'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
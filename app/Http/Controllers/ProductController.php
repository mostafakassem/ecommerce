<?php

namespace App\Http\Controllers;

use App\Addition;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Return_;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where(function ($q) {
            if (request()->input('search')) {
                $q->where('name', 'like', '%' . request()->search . '%');
                $q->orWhere('description', 'like', '%' . request()->search . '%');
            }
        })->get();
//        $products = Product::with("category")->get();

//           return $products[2]->category->parent;
        $categories = Category::where('parent_id',0)->get();
        return view('index',compact('products','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $additions = Addition::all();
        $categories = Category::where('parent_id',0)->get();
        $items = Category::where('parent_id','!=',0)->get();
        return view('create',compact('categories','items','additions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $items =[
            'name' => 'required',
            'category_name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'image' => 'required'
        ];
        $message =[
            'name.required'=>'اسم المنتج مطلوب',
            'category_name.required'=>' التصنيف الرئيسي مطلوب',
            'description.required'=>' الوصف مطلوب',
            'price.required'=>' السعر مطلوب',
            'image.required'=>'صوره المنتج مطلوبه'

        ];
        $this->validate($request,$items,$message);
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->category_id = $request->branch;
        $product->hide = $request->hide  ?? "hide";
//dd($request->pic);
        if ( $request->hasFile('pic')  ) {
//            dd($request->pic);
            $image = $request->pic;
            $image_new_name = time() . $image->getClientOriginalName();
            $image->move(public_path().'/product', $image_new_name);
            $product->image = $image_new_name;

        }
        $product->save();
        $add = $request->addition_name;
        //dd($add);
        $product->additions()->sync($add);
        return redirect(url('product'))->with('success','تمت الاضافه بنجاح');

        // ==========
//        $add = [1,2] ;
//
//        foreach ( $add as $i  ){
//            $data = ["product_id"=>$product->id , 'addition_id'=>$i] ;
//            Addition::create($data) ;
//        } ;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Product::findOrFail($id);
//        dd($model);
        $maincats = $model->category->parent->where('parent_id',0)->get();
//        dd($maincats);
//      $maincats = Category::where('parent_id',0)->get();
        $branchcats = Category::where('parent_id','!=',0)->where('parent_id',$model->category->parent_id)->get();
//        dd($branchcats);
        $additions = Addition::all();

        $ss = $model->additions()->pluck('addition_id')->toArray();
//        dd($ss);

        return view('edit',compact('model' , 'maincats','branchcats','additions','ss'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Product::findOrFail($id);
//        dd($request->all());
//        dd($id);
//        return $data;
        $rules = $this->validate(request(),[
            'name' => 'required',
            'category_name' => 'required',
            'price' =>'required'

        ]);
//        $data->name = $request->name;
        $data->category_id = $request->category_name1;
        $data->hide = $request->radio;
        $data->description = $request->description;
        $data->price = $request->price;


        if($request->pic != ''){
            $path = public_path().'/product/';

            //code for remove old file
            if($data->image != ''  && $data->image != null){
                $image_old = $path.$data->image;
                unlink($image_old);
            }

            //upload new file
            $image = $request->pic;
            $imagename = $image->getClientOriginalName();
            $image->move($path, $imagename);

            //for update in table
            $data->update(['image' => $imagename]);
        }

        $data->update($rules);

          $add = $request->addition_name;
        $data->additions()->sync($add);



        return redirect(route('product.index'))->with('success','تم التعديل بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//dd(2);
        $data = Product::findOrFail($id);
        $data->delete();

        $data->additions()->detach();
        return redirect(route('product.index'))->with('success','تم الحذف بنجاح');
    }

public function products(){
        $products = Product::where(function ($q) {
        if (request()->input('search')) {
            $q->where('name', 'like', '%' . request()->search . '%');
            $q->orWhere('description', 'like', '%' . request()->search . '%');
        }
    })->get();
//        $products = Product::with("category")->get();
//        return $products[2]->category->parent;

    $categories = Category::where('parent_id',0)->get();
    return view('index2',compact('products','categories'));

}

}

<?php

namespace App\Http\Controllers;

use App\Addition;
use App\AdditionProduct;
use App\Category;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Stmt\Return_;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        dd(222);
      $user = Auth::user();
      return view('completerequest2',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $user = User::findOrFail($id);

        $rules = $this->validate(request(),[
            'lat' => 'required',
            'long' => 'required',
               ]);
        $user->latitude = $request->lat;
        $user->longitude = $request->long;
        $user->update($rules);

        return redirect()->back()->with('success','تم التأكيد ع الطلب بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

//    public function get_user_login()
//    {
//        if (Auth::guard('user')->check())
//        {
//            return redirect(route('getproduct'));
//        }
//        else {
//            return view('auth.login');
//        }
//    }
//    public function post_user_login()
//    {
////        dd(Hash::make('123'));
//        $user = User::where('email', \request('email'))->first();
//
//        $card = ['email'=>\request('email'), 'password'=>\request('password')];
//
//        if (\auth()->guard('user')->attempt($card, false))
//        {
//            \auth()->guard('user')->login($user);
//            return redirect(route('getproduct'));
//        } else {
//            return back()->with('error','يوجد خطا في البيانات الرجاء التاكد من البريد الالكتروني وكلمه المرور');
//        }
//    }
//
//    public function logout(){
//
//        auth()->guard('user')->logout();
//        return redirect()->route('login');
//    }
//













}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

//use Illuminate\Database\Eloquent\SoftDeletes;



class Order extends Model
{
    protected $guarded = ['id'];

    public function OrderProducts()
    {
        return $this->hasMany(OrderProduct::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }


}

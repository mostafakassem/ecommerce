<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

//use Illuminate\Database\Eloquent\SoftDeletes;



class OrderProduct extends Model
{
    protected $guarded = ['id'];

    public function Product(){
        return $this->belongsTo(Product::class);
    }
}

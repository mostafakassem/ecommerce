<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

//use Illuminate\Database\Eloquent\SoftDeletes;



class Product extends Model
{
    protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function additions()
    {
        return $this->belongsToMany(Addition::class);
    }
    public  function favourite_to_users(){

        return $this->belongsToMany(User::class,'favourites')->withTimestamps();

    }
    public function Order(){
        return $this->hasMany(Order::class);
    }


}

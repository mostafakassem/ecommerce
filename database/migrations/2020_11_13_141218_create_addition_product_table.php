<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditionProductTable extends Migration
{
    public function up()
    {
        Schema::create('addition_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('addition_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('addition_product');
    }
}
<?php


return [

  'add' => 'add product',
    'product' => ' product name ',
    'category' => 'category name',
    'process' => 'AllProcess',
    'msg' => 'Are You Sure To Delete This Product',
    'edit' => 'edit',
    'delete' => 'delete',
    'branch' => 'category branch',
    'select' => 'select',
    'image' => 'image',
    'price' => 'price',
    'show' => 'show product',
    'yes' => 'yes',
    'no' => 'no',
    'description' => 'description',
    'submit' => 'submit',
    'addition' => 'addition',

];
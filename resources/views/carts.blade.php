@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>E-commerce grid</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>E-commerce</a>
                </li>
                <li class="active">
                    <strong>Products grid</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-4">


        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {{--{{dd($cartItems)}}--}}

        <div class="row">

            @foreach($cartItems as $cartItem)
                <div class="col-md-3">
                    <div class="ibox">
                        <div class="ibox-content product-box">

                            <div class="product-imitation">
                                <img src="{{ asset('/public/product/'.$cartItem->attributes->image)}}"alt="no_pic_exist" class="img-responsive" >
                            </div>
                            <div class="product-desc">
                                <span class="product-price">
                                    {{$cartItem->price}}
                                </span>
                                                               {{--<a href="{{url("/sub-category/{$cartItem->category_id}")}}" class="text-muted" >{{$cartItem->category->parent->category_name}}</a>--}}
                                <p  class="product-name"> {{$cartItem->name}}</p>
                                {{--<span>{{$cartItem->quantity}}</span>--}}
                                <form method="get" action="{{route('cart.edit',$cartItem->id)}}">
                                    @csrf
                                    {{method_field('GET')}}
                                 <input type="number" style="color: #1a7bb9"value="{{$cartItem->quantity}}" name="quantity">
                                    <button type="submit" class="btn btn-success btn-sm">edit</button>
                                </form>
                                <div class="m-t text-right">

                                    <a href="{{url('product/'.$cartItem->id)}}" class="btn btn-xs btn-outline btn-primary">تفاصيل المنتج</a>
                                    {{--<a class="btn btn-success btn-sm" onclick="document.getElementById('favourite-form-{{$cartItem->id}}').submit();">--}}
                                        {{--<i class="fa fa-star"></i> {{$cartItem->favourite_to_users->count()}} </a>--}}

                                    <form id="favourite-form-{{$cartItem->id}}" action="{{url('favourite/'.$cartItem->id)}}" method="get" style="display: none">
                                    </form>
                                    <a href="{{route('cart.show',$cartItem->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-cart-plus"></i> Remove cart</a>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach


        </div>
        <a type="button" class="btn-success btn-rounded confirm" href="{{url("/confirm")}}">تأكيد الطلب</a>
        </div>


@stop
@section("styles")
    <style>
        .product-imitation > img{
            height: 300px;
            width: 100%;

        }
        .product-imitation {
            padding:  0 ;
        }
        .col-lg-4  a{
            position: absolute;
            margin-top: 20px;
            width: 80px;
            text-align: center;

        }

        .confirm{
            margin:30px 550px 0px 200px;
            width: 200px;
            border-radius: 3px;
        }
    </style>
@endsection



@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>E-commerce grid</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>E-commerce</a>
                </li>
                <li class="active">
                    <strong>Products grid</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-4">


        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {{--{{dd($cartItems)}}--}}

        <div class="row">

            @foreach($cartItems as $cartItem)
                {{--<form action="">--}}
                <div class="col-md-3">
                    <div class="ibox">
                        <div class="ibox-content product-box">

                            <div class="product-imitation">
                                <img src="{{ asset('/public/product/'.$cartItem->attributes->image)}}"alt="no_pic_exist" class="img-responsive" >
                            </div>
                            <div class="product-desc">
                                <span class="product-price">
                                    {{$cartItem->price}}
                                </span>
                                {{--<a href="{{url("/sub-category/{$cartItem->category_id}")}}" class="text-muted" >{{$cartItem->category->parent->category_name}}</a>--}}
                                <p  class="product-name"> {{$cartItem->name}}</p>
                                {{--<span>{{$cartItem->quantity}}</span>--}}

                                <span  style="color: #1a7bb9">{{$cartItem->quantity}}</span>


                                <div class="m-t text-right">

                                    <a href="{{url('product/'.$cartItem->id)}}" class="btn btn-xs btn-outline btn-primary">تفاصيل المنتج</a>
                                    {{--<a class="btn btn-success btn-sm" onclick="document.getElementById('favourite-form-{{$cartItem->id}}').submit();">--}}
                                    {{--<i class="fa fa-star"></i> {{$cartItem->favourite_to_users->count()}} </a>--}}

                                    <form id="favourite-form-{{$cartItem->id}}" action="{{url('favourite/'.$cartItem->id)}}" method="get" style="display: none">
                                    </form>
                                    <a href="{{route('cart.show',$cartItem->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-cart-plus"></i> Remove cart</a>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
         </form>
            @endforeach


        </div>

        <div class="clearfix"></div>
        <div class="col-lg-4 col-md-offset-4">
            <div class="panel-body text-center">
                <p>الاجمالي<span style="font-weight: bold"> {{$total}} </span></p>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-4 col-md-offset-4 text-center ">
            <form action="{{route('order.store')}}" method="post">
                {{csrf_field('POST')}}
            <div><label>نوع الدفع</label>
                <input type="radio"value="visa" name="payment_way"> فيزا
                <input type="radio"value="on_deliver" name="payment_way">   عندالاستلام
                <div class="clearfix"></div>
                <button type="submit" class="btn btn-info">استكمال الطلب</button>
            </div>
            </form>
        </div>


@stop
@section("styles")
    <style>
        .product-imitation > img{
            height: 300px;
            width: 100%;

        }
        .product-imitation {
            padding:  0 ;
        }
        .col-lg-4  a
        {
            position: absolute;
            margin-top: 20px;
            width: 80px;
            text-align: center;
        }
        .panel-body
        {
            background-color: #1AB394;
            color: #1b1b1b;
        }
        .wrapper-content {
            padding: 20px 10px 57px;

        }
    </style>
@endsection












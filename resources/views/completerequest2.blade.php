@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>E-commerce grid</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>E-commerce</a>
                </li>
                <li class="active">
                    <strong>Products grid</strong>
                </li>
            </ol>
        </div>
    </div>
{{--{{dd($user->id)}}--}}
    <div class="" style="margin: 40px 0px 20px;">
        @include('alerts')
        <form action="{{route('user.update',$user->id)}}" method="post" class="form-horizontal">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group"><label class="col-sm-2 control-label">الاسم</label>

                <div class="col-sm-10"><input type="text" value="{{$user->name}}" class="form-control"></div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">البريد الالكتروني</label>
                <div class="col-sm-10">
                    <input type="text" value="{{$user->email}}" class="form-control">
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">الخريطه</label></div>
            <div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Location:</label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="us3-address" />
                    </div>

                    <label class="col-sm-2 control-label" style="margin-right: 0px">Radius:</label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="us3-radius" />
                    </div>
                </div>
            </div>

                <div  id="somecomponent" style="width: 800px; height: 400px;margin-right: 110px"></div>
                <div class="m-t-small">
                    <label class="p-r-small col-sm-1 control-label">Lat.:</label>

                    <div class="col-sm-3">
                        <input name="lat" type="text" class="form-control" style="width: 110px" id="us3-lat" />
                    </div>
                    <label class="p-r-small col-sm-2 control-label">Long.:</label>

                    <div class="col-sm-3">
                        <input name="long" type="text" class="form-control" style="width: 110px" id="us3-lon" />
                    </div>
                </div>

            <div>
                <button class="btn btn-success btn-rounded confirm">تأكيد</button>
            </div>

        </form>
    </div>

@stop
@section("styles")
    <style>
        .confirm
        {
            width: 100px;
            margin: 30px 550px 0px 200px;
            border-radius: 3px;

        }

    </style>
@endsection
@push('scripts')
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=ar'></script>
    <script src="{{asset('assets/js/locationpicker.jquery.js')}}"></script>

    <script>
        $('#somecomponent').locationpicker();


    </script>
@endpush



<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Dashboard v.4</title>

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap.rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style1.css')}}" rel="stylesheet">
    <style>
        .ss{
            margin-right: 500px;
            float: left;
        }
        .lang{
            float: left;
            margin-left: 80px;
            margin-top: -43px;
            font-size: 18px;

        }
        #arabic
        {
            color: red;
            border-left: 2px solid #1b6d85;
            padding: 0 .5rem;
        }
        #en
        {
            padding: 0 .5rem;
            color: #1b1b1b;

        }

    </style>

</head>

<body class="top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">Inspinia</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">

                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a class="ss" href="#" onclick="document.getElementById('logout-form1').submit();">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                    <form id="logout-form1" action="{{ route('logout') }}" method="get" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="lang">
                    <span> <a id="arabic" href="{{url('lang/ar')}}">عربي</a></span>
                    <span> <a id="en" href="{{url('lang/en')}}">EN</a></span>
                </div>
            </nav>
        </div>
    <div class="ibox-content">
        @include('alerts')
        <form action="{{url('product')}}" method="post"  class="form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}

            {{--create parent_id--}}
            {{--<input type="hidden" name="parent_id"  class="form-control">--}}

            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.product')}}</label>

                <div class="col-sm-10"><input type="text" name="name" class="form-control" value="{{old('name')}}"></div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.category')}}</label>
                <div class="col-sm-10">
                    <select class="form-control m-b" name="category_name" id="mainCategory"  >
                        <option value="" disabled selected hidden>{{__('message.select')}}</option>
                        @foreach($categories as $category)
                            <option  value="{{$category->id}}" {{ (collect(old('category_name'))->contains($category->id)) ? 'selected':'' }}>{{$category->category_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.branch')}}</label>
                <div class="col-sm-10">
                    <select class="form-control m-b" name="branch" id="branch" >
                        <option value="" disabled selected hidden>{{__('message.select')}}</option>

                        {{--@foreach($items as $item)--}}
                            {{--<option value="{{$item->category_name}}">{{$item->category_name}}</option>--}}
                        {{--@endforeach--}}

                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group"><label class="col-sm-2 control-label">{{__('message.image')}}</label>

                    <div class="col-sm-10"><input type="file" name="pic" class="form-control"></div>
                </div>

            </div>
            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.price')}}</label>

                <div class="col-sm-10"><input type="number" name="price" class="form-control" value="{{old('price')}}"></div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.show')}}</label>
                <div class="col-sm-10">

                        <label>
                            <input type="radio" value="not_hide"  name="hide" {{(old('hide') == 'not_hide') ? 'checked' : ''}}> {{__('message.yes')}}
                             </label>
                        <label>
                            <input type="radio"  value="hide"  name="hide" {{(old('hide') == 'hide') ? 'checked' : ''}}> {{__('message.no')}}
                        </label>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                        <div class="ibox-title">
                            <h3>{{__('message.description')}}</h3>
                            <div class="ibox-tools">
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="ibox-content no-padding">
                            <textarea class="summernote form-group" cols="10" rows="10" name="description">
                                {{old('description')}}
                            </textarea>
                        </div>
                </div>
            </div>
            <div class="row">
            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.addition')}}</label>
             @foreach($additions as $addition)
                 <input type="checkbox" value="{{$addition->id}}" name="addition_name[]" {{ ( is_array(old('addition_name')) && in_array($addition->id, old('addition_name')) ) ? 'checked ' : '' }}>
                    {{$addition->name}}
             @endforeach
            </div>
            </div>
            <div class="text-center">
                <button  type="submit" class="btn btn-primary btn-rounded bg-success" >
                    {{__('message.submit')}}
                </button>
            </div>
        </form>
    </div>
    {{--@stop--}}
@push('scripts')
    <!-- SUMMERNOTE -->
    <script src="{{asset('assets/js/plugins/summernote/summernote.min.js')}}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>


    <script>

    $(document).ready(function(){

        $('.summernote').summernote();


        $("#mainCategory").change(function (e) {
            e.preventDefault();

            var catId = $(this).val();
              // console.log(catId)
            // let data = {catId}
            axios.get('{{url('child')}}'+'/' + catId)
                .then(function (response) {
                    console.log(response.data.status);

                    if (response.data.status == true){
                        // var tt = "" ;
                        $("#branch").empty().append(response.data.data)

                                            }



                })
                // .catch(function (error) {
                //     console.log(error);
                // })
                // .then(function () {
                //     // always executed
                // });



        });

    });
</script>
@endpush


@push('styles')
    <link href="{{asset('assets/css/css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <style>
        .note-editor.note-frame .note-editing-area .note-editable{
            height: 200px;
        }
    </style>
@endpush

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Edit Product</title>

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap.rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style1.css')}}" rel="stylesheet">
    <style>
        .ss{
            margin-right: 500px;
            float: left;
        }
        .lang{
            float: left;
            margin-left: 80px;
            margin-top: -43px;
            font-size: 18px;

        }
        #arabic
        {
            color: red;
            border-left: 2px solid #1b6d85;
            padding: 0 .5rem;
        }
        #en
        {
            padding: 0 .5rem;
            color: #1b1b1b;

        }

    </style>

</head>

<body class="top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">Inspinia</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">

                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a class="ss" href="#" onclick="document.getElementById('logout-form1').submit();">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                    <form id="logout-form1" action="{{ route('logout') }}" method="get" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="lang">
                    <span> <a id="arabic" href="{{url('lang/ar')}}">عربي</a></span>
                    <span> <a id="en" href="{{url('lang/en')}}">EN</a></span>
                </div>
            </nav>
        </div>
    <div class="ibox-content">
@include('alerts')

        <form action="{{route('product.update',$model->id)}}" method="post"  class="form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.product')}}</label>

                <div class="col-sm-10"><input type="text"  name="name" class="form-control" value="{{$model->name}}"></div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.category')}}</label>
                <div class="col-sm-10">
                    <select class="form-control m-b"   name="category_name" id="mainCategory">
                        @foreach( $maincats as $maincat )
                            <option value="{{$maincat->id}}" {{( $maincat->id == $model->category->parent->id ) ? "selected":""}} >{{$maincat->category_name}}</option>
                        @endforeach
                    </select>
                </div>
{{--{{ dd($branchcat->id/, $model->category_id ) }}--}}
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.branch')}}</label>
                <div class="col-sm-10">

                    <select class="form-control m-b"  id="branch"   name="category_name1">

                        @foreach( $branchcats as $branchcat )
                            <option value="{{$branchcat->id}}" {{( $branchcat->id == $model->category_id ) ? "selected":""}} >{{$branchcat->category_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group"><label class="col-sm-2 control-label">{{__('message.image')}}</label>

                    <div class="col-sm-10">
                        <input type="file" name="pic" class="form-control">
                        <img src="{{ asset('/public/product/'.$model->image) }}" style="height: 50px">
                    </div>
                </div>

            </div>
            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.price')}}</label>

                <div class="col-sm-10"><input type="text" name="price" class="form-control" value="{{$model->price}}"></div>
            </div>


            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.show')}}</label>
                <div class="col-sm-10">

                    <label>
                        <input type="radio" value="not_hide" {{($model->hide=="not_hide") ? "checked":""}} id="" name="radio"> نعم
                    </label>
                    <label>
                        <input type="radio" value="hide" {{($model->hide=="hide") ? "checked":""}} name="radio"> لا
                    </label>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>{{__('message.description')}}</h3>
                            <div class="ibox-tools">
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <div class="ibox-content no-padding">

                            <textarea name="description" class="summernote" class="form-group">
                                {{$model->description}}
                            </textarea>

                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">{{__('message.addition')}}</label>
                @foreach($additions as $addition)
                    <input type="checkbox" {{(in_array($addition->id, $ss))?"checked":""}} value="{{$addition->id}}" name="addition_name[]">
                        {{$addition->name}}
                @endforeach
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary btn-rounded bg-success">
                    {{__('message.submit')}}
                </button>
            </div>
        </form>
    </div>
{{--@stop--}}
{{--@push('scripts')--}}
    <script src="{{asset('assets/js/plugins/summernote/summernote.min.js')}}"></script>
<script>

    $(document).ready(function() {

            $('.summernote').summernote();

        $("#mainCategory").change(function (e) {
            e.preventDefault();

            var catId = $(this).val();
            // console.log(catId)
            // let data = {catId}
            axios.get('{{url('child')}}'+'/' + catId)
                .then(function (response) {
                    console.log(response.data.status);

                    if (response.data.status == true){
                        // var tt = "" ;
                        $("#branch").empty().append(response.data.data)

                    }



                })
            // .catch(function (error) {
            //     console.log(error);
            // })
            // .then(function () {
            //     // always executed
            // });



        });

    })
</script>
{{--@endpush--}}
@push('styles')
    <link href="{{asset('assets/css/css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">

@endpush





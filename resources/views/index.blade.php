<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Products</title>

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap.rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style1.css')}}" rel="stylesheet">
    <style>

        .ss{
            margin-right: 500px;
            float: left;
        }

        .lang{
            float: left;
            margin-left: 80px;
            margin-top: -43px;
            font-size: 18px;

        }
        #arabic
        {
            color: red;
            border-left: 2px solid #1b6d85;
            padding: 0 .5rem;
        }
        #en
        {
            padding: 0 .5rem;
            color: #1b1b1b;

        }

    </style>

</head>

<body class="top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">Inspinia</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">

                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a class="ss" href="#" onclick="document.getElementById('logout-form1').submit();">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                    <form id="logout-form1" action="{{ route('logout') }}" method="get" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>

                <div class="lang">
                    <span> <a id="arabic" href="{{url('lang/ar')}}">عربي</a></span>
                    <span> <a id="en" href="{{url('lang/en')}}">EN</a></span>
                </div>
            </nav>
        </div>
    <div class="wrapper wrapper-content">
        <div class="col-md-4 col-md-push-8">
            <a type="button" class="btn btn-primary btn-rounded bg-success" href="{{route('product.create')}}">
                {{__('message.add')}}
            </a>

        </div>
    </div>
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            @include('alerts')
            <div class="ibox-content">

                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">{{__('message.product')}}</th>
                        <th scope="col">{{__('message.category')}}</th>

                        <th scope="col">{{__('message.process')}}</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                    <tr>
                        <th scope="row">
                            {{$product->name}}
                        </th>
                        <th scope="row">
                            {{--دلوقتي عندي التصنيفات كلها ف عمود واحد وعاوزاعرض بس التصنيفات الاساسيه عشان كدا عملت فانكشن جوه التصنيفات اسمها parent و دي بتنادي علي نفسها ومنها جبت التصنيفات الاساسيه فقط--}}

                            {{( $product->category ) ? $product->category->parent['category_name']:"لا يوجد" }}
                        </th>
                        <td>
                            <a type="button" class="btn-info btn-rounded" href="{{url("/product/{$product->id}/edit	")}}">{{__('message.edit')}}</a>
                            <a href="#" class="btn-danger btn-rounded delete" data-id="{{$product->id}}">{{__('message.delete')}}</a>
                        </td>

                    </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="modal inmodal" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="delet modal-content animated bounceInRight" style="padding: 1em">
                            <form action="" method="post" id="deleteform">
                                @csrf
                                {{method_field('DELETE')}}
                                <p>{{__('message.msg')}}</p>
                                <button type="submit" class="btn btn-danger">{{__('message.yes')}}</button>
                                <button type="button" class="btn btn-warning" data-dismiss="modal">{{__('message.no')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@push('scripts')
<script>

$(document).on("click",".delete", function (e) {
    e.preventDefault()
    // alert("asasa") ;
    // console.log("dsdsdss")

// alert();
/* get data from button */
var id = $(this).data("id");
// console.log("id",id);
/* set action attribute to new url */
$('#deleteform').attr('action', '{{url("product")}}' + '/' + id);
/* open the modal */
$('#deleteModal').modal('show');
})
</script>
@endpush
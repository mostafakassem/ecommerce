@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>E-commerce grid</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>E-commerce</a>
                </li>
                <li class="active">
                    <strong>Products grid</strong>
                </li>
            </ol>
        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                @foreach($products as $product)
                <div class="col-md-3">
                    <div class="ibox" id="{{$product->id}}">
                        <div class="ibox-content product-box">

                            <div class="product-imitation">
                                <img src="{{ asset('/public/product/'.$product->image) }}"alt="no_pic_exist" class="img-responsive" >
                            </div>
                            <div class="product-desc">
                                <span class="product-price">
                                    {{$product->price}}
                                </span>
                                <a href="{{url("/sub-category/{$product->category_id}")}}" class="text-muted" >{{$product->category->parent->category_name}}</a>
                                <p  class="product-name"> {{$product->name}}</p>
                                <div class="m-t text-right">

                                    <a href="{{url('pro/'.$product->id)}}" class="btn btn-xs btn-outline btn-primary">تفاصيل المنتج</a>
                                    <a class="btn btn-success btn-sm favourite" data-id="{{$product->id}}" ><i class="fa fa-star"></i>{{$product->favourite_to_users->count()}}</a>
                                    <a href="" class="btn btn-warning btn-sm addCart1"
                                       data-id="{{$product->id}}"
                                       data-name="{{$product->name}}"
                                       data-image="{{asset('/public/product/'.$product->image) }}"
                                       data-price="{{$product->price}}"
                                    ><i class="fa fa-cart-plus"></i> Add to cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        <div class="modal inmodal" id="addModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                    <form class="form-horizontal" id="addform">
                                @csrf
                                {{method_field('POST')}}
                        <div class="ibox" >
                                    <div class="ibox-content product-box" >

                                        <div class="product-imitation">
                                            <img id="image"  src="image"alt="no_pic_exist" class="img-responsive" >
                                        </div>
                                        <div class="product-desc">
                                <input class="product-price" name="price" id="price">

                                <input  class="product-name1" id="name">
                                            <div class="input-group bootstrap-touchspin a">
                                                <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                                <input class="touchspin3 form-control n" type="number" id="quantity" min="1" value="1" name="quantity"   style="display: block;">
                                                <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                                <span class="input-group-btn-vertical">
                                                  </span></div>

                                            <div class="m-t text-right">
                                                <button type="submit" class="btn btn-success btn-sm m add"
                                                ><i class="fa fa-cart-plus"></i> Add to cart</button>
                                                <a href="" class="btn btn-danger btn-sm b">cancel</a>

                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </form>

            </div>
        </div>

    </div>


@stop
@section("styles")
    <style>
        .product-imitation > img{
            height: 300px;
            width: 100%;

        }
        .product-imitation {
            padding:  0 ;
        }
        .col-lg-4  a{
            position: absolute;
            margin-top: 20px;
            width: 80px;
            text-align: center;

        }
    </style>
@endsection
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>


        $(document).on("click",".favourite", function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            // console.log('>>>>>>>>>', id);
            axios.get('{{url('/favourite')}}' + '/' + id, {
                _method: 'Get'
            })
                .then(response => {
                    // console.log("users>>>>>>>>",response.data.p)
                    console.log("response ??????? ", response)
                    console.log("response ?>> ", response.data.id)
                    console.log("response===> ", response.data.item)
                    $("#"+response.data.id).find(".favourite").empty().append(response.data.p)

                });

        })

        $(document).on("click",".addCart", function (e) {
            e.preventDefault()
            // alert("asasa") ;

            /* get data from button */
            var id = $(this).data("id");
            var name = $(this).data("name");
            var image = $(this).data("image");
            var price = $(this).data("price");


            // console.log("id>>>>>>>>>",id);
            // console.log("quantity>>>>>>>>>",quantity);
            //   console.log("image>>>>>>>>>",image);

              $("#image").attr("src",image);
              // console.log("price>>>>>>>>>",price);
              // console.log("id>>>>>>>>>",id);
            $("#id").val(id);
            $("#name").val(name);
            $("#price").val(price);
            $('#quantity').on('change', function(){
                var new_price = $(this).val()*price;
               $("#price").val(new_price);
                // alert(new_price);
            });
            $("#image").val(image);



            /* set action attribute to new url */
            $('#addform').attr('action', '{{url("/cart")}}' + '/' + id);
            /* open the modal */
            $('#addModal').modal('show');
        })


        $(document).on("click",".addCart1", function (e) {
            e.preventDefault()

             // alert("asasa") ;

            /* get data from button */
            var id = $(this).data("id");
            var name = $(this).data("name");
            var image = $(this).data("image");
            var price = $(this).data("price");
              $("#image").attr("src",image);
            // console.log("id>>>>>>>>>",id);
            $("#id").val(id);
            $("#name").val(name);
            $("#price").val(price);
            $('#quantity').on('change', function(){
                var new_price = $(this).val()*price;
                $("#price").val(new_price);
                // alert(new_price);
            });
            $("#image").val(image);
            $('#addModal').modal('show');
            $('.add').data('myid', id);
        })
        $(document).on("click",".add", function (e) {
            e.preventDefault()
            // alert("asssssssss");

            var id = $(this).data("myid");
           var quantity = $('#quantity').val();
            // console.log("id>>>>>>>>>>>>",id)


            axios.post('{{url('/cart')}}' + '/' + id , {
                 quantity:quantity,
                _method: 'PUT'
        })
            .then(response => {
                console.log("CART>>>>>>>>","success");
                console.log("cccccc>>>>",response.data.item);
                alert("تمت الاضافه بنجاح");
                $('#addModal').modal('hide');


            })
            })

    </script>
@endpush

@push('styles')

    <style>
        .product-name1{
            padding: 6px 12px;
            position: absolute;
            top: 1px;
            right: 0;
            background-color: #0e9aef;
        }
        .product-desc{
            padding: 10px;
        }
        .m{
            margin: 60px 150px 0px 0px;

        }
        .b{
            margin: 60px 20px 0px 0px;

        }
        .n{
            height: 40px;
            border-color: #0a6aa1;
        }
        .a{
           position: absolute;
            display: block;
            margin: 30px 100px 20px 0px !important;

        }

    </style>
@endpush


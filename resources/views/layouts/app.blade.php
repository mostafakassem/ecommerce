<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title></title>

    @include('layouts.styles')
</head>
<body>

    @include('layouts.admin-nav')
    </div>
    @yield('content')

</div>
@yield('styles')
@include('layouts.scripts')
@yield('scripts')

</body>
</html>
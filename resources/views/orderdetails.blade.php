@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>E-commerce grid</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>E-commerce</a>
                </li>
                <li class="active">
                    <strong>Products grid</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-4">


        </div>
    </div>
{{--{{dd($order)}}--}}
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title ">
                    <div class="row">
                        <div class="col-lg-8">
                            <h5 style="font-size: 30px"> تفاصيل الطلب رقم {{$order->id}}</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="img text-center" >
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="ibox float-e-margins">
                                        <div class="col-sm-12">
                                            <div class="col-sm-4"><h4>اسم العميل </h4></div>
                                            <div class="col-sm-8"><p>{{$order->user->name}} </p></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-4"><h4>طريقه الدفع</h4></div>
                                            <div class="col-sm-8"><p>{{$order->payment_way}}</p></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-4"><h4> القيمه المضافه </h4></div>
                                            <div class="col-sm-8"><p>{{$order->add_value}}</p></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-4"><h4>  المبلغ المطلوب </h4></div>
                                            <div class="col-sm-8"><p>{{$order->net}}</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
























@stop
@section("styles")
            <style>
            </style>
@endsection

@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>E-commerce grid</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>E-commerce</a>
                </li>
                <li class="active">
                    <strong>Products grid</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-4">
        </div>
    </div>
    <div class="row">
        @include('alerts')
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>رقم الطلب</th>
                            <th>طريقه الدفع</th>
                            <th>اجمالي السعر</th>
                            <th> اجمالي الخصم</th>
                            <th>الاجمالي بعد الخصم</th>
                            <th> عمليات</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->payment_way}}</td>
                                <td>{{$order->OrderProducts->sum('price')}}</td>
                                <td> {{$order->OrderProducts->sum('discount')}} </td>
                                <td>{{$order->OrderProducts->sum('after_discount')}}</td>
                                <td>
                                    <button type="button" class="btn btn-success"><a
                                                href="{{url('/order/'.$order->id)}}"
                                                style="color: inherit;">عرض</a></button>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


























@stop
@section("styles")
    <style>
    </style>
@endsection

@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>E-commerce grid</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>E-commerce</a>
                </li>
                <li class="active">
                    <strong>Products grid</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-4">
            <a type="button" href="{{route('product.create')}}" class="btn-rounded btn-success " >Add </a>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            @foreach($cats as $cat)
                @foreach( $cat->product as $i )
                <div class="col-md-3">
                    <div class="ibox">
                        <div class="ibox-content product-box">

                            <div class="product-imitation">
                                <img src="{{ asset('/public/product/'.$i->image) }}"alt="no_pic_exist" class="img-responsive" >
                            </div>
                            <div class="product-desc">
                                <span class="product-price">
                                    {{$i->price}}
                                </span>
                                <p href="{{url("/sub-category/{$i->category_id}")}}" class="text-muted" >{{$cat->parent->category_name}}</p>
                                <p  class="product-name"> {{$i->name}}</p>
                                <div class="small m-t-xs">
                                    {{$i->description}}
                                </div>
                                <div class="m-t text-right">

                                    <a href="{{url('product/'.$i->id)}}" class="btn btn-xs btn-outline btn-primary">تفاصيل المنتج</a>
                                    <a href="{{url("/product/{$i->id}/edit")}}" class="btn btn-xs btn-outline  btn-warning">Edit </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            @endforeach
        </div>
    </div>


@stop
@section("styles")
    <style>
        .product-imitation > img{
            height: 300px;
            width: 100%;

        }
        .product-imitation {
            padding:  0 ;
        }
        .col-lg-4  a{
            position: absolute;
            margin-top: 20px;
            width: 80px;
            text-align: center;

        }
    </style>
@endsection



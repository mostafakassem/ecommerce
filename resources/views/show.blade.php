@extends('layouts.app')

@section('content')



        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>E-commerce product detail</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{route('product.index')}}">Home</a>
                    </li>
                    <li>
                        <a>E-commerce</a>
                    </li>
                    <li class="active">
                        <strong>Product detail</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">

                    <div class="ibox product-detail">
                        <div class="ibox-content">

                            <div class="row">
                                <div class="col-md-5">


                                    <div class="product-images">

                                        <div>
                                            <div class="image-imitation">
                                                <img src="{{ asset('/public/product/'.$model->image) }}" >
                                            </div>
                                        </div>


                                    </div>

                                </div>
                                <div class="col-md-7">

                                    <h2 class="font-bold m-b-xs">
                                        {{$model->name}}
                                    </h2>

                                    <div class="m-t-md">
                                        <h2 class="product-main-price">{{$model->price}}  </h2>
                                    </div>
                                    <hr>

                                    <h4>Product description</h4>

                                    <div class="small text-muted">
                                        {!!$model->description!!}
                                    </div>

                                    <hr>

                                    <div>
                                        <div class="btn-group">


                                            <button class="btn btn-white btn-sm"><i class="fa fa-envelope"></i> Contact with author </button>
                                        </div>
                                    </div>



                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </div>

   @stop
@section("styles")
    <style>
        .image-imitation > img{
            height: 100%;
            width: 100%;

        }
        .image-imitation {
            padding:  0px 0 ;
        }
    </style>
@endsection





<script>
    $(document).ready(function(){


        $('.product-images').slick({
            dots: true
        });

    });

</script>


@extends('layouts.app')
@section('content')
    <div class="wrapper wrapper-content">
        <div class="col-md-4 col-md-push-8">

        </div>
    </div>
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            {{--@include('alerts')--}}
            <div class="ibox-content">

                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">اسم  التصنيف  </th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($cats as $cat)
                        <tr>
                            <td scope="row">
                                {{$cat->category_name}}
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@stop


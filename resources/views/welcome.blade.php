<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Ecommerce</title>

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap.rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <style>
        .loginscreen.middle-box{
            width:auto;
            /*background-color:#1b6d85;*/
        }
        img{
            -webkit-box-shadow: 0px 10px 5px 1px rgba(0,0,0,0.15);
            -moz-box-shadow: 0px 10px 5px 1px rgba(0,0,0,0.15);
            box-shadow: 0px 10px 5px 1px rgba(0,0,0,0.15);
        }
        .logo-name{
            color: #e6e6e6;
            font-size: 100px;
            font-weight: 800;
            letter-spacing: -10px;
            margin-bottom: 0;
            text-align: center;
        }

    </style>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h3 class="logo-name"> Ecommerce</h3>
        </div>

        <a href="{{route('login')}}">
            <img src="{{asset('public/product/eCommerce.png')}}" class="img-responsive img-circle" alt="قطرة ماء">
        </a>

    </div>
    <h3>اهلا بكم متعه التسوق</h3>

</div>

<!-- Mainly scripts -->
<script src="{{asset('assets/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
</body>
</html>

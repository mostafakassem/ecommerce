<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('home', function(){
//    return redirect('/');
//});


Route::get('/', function(){
    return view('welcome');
});


 Route::middleware('check-auth-user')->group(function () {

    Route::get('/products', 'ProductController@products')->name('getproduct');
    Route::get('sub-category/{id}', 'CategoryController@sub')->name('sub-category');
    Route::get('prod/{id}', 'CategoryController@prod');
    Route::resource('cart', 'CartController');
    Route::get('/confirm', 'CartController@confirm');
    Route::resource('/user', 'UserController');
    Route::resource('/order', 'OrderController');
    Route::get('/favourite/{product}','FavouriteController@add2')->name('product.favourite2');
    Route::get('pro/{id}','CategoryController@showproduct');
});


    Route::get('admin/login','AdminController@get_admin_login');
    Route::post('admin/login','AdminController@post_admin_login')->name('post-login');



Auth::routes();

Route::middleware('localization')->group(function () {
    Route::get('lang/{locale}', function ($locale){
        session()->put('locale',$locale);
        return redirect()->back();

    });

    Route::middleware('check-auth-admin')->group(function () {
        Route::get('/logout', 'AdminController@logout')->name('logout');
        Route::resource('product', 'ProductController');
        Route::get('child/{id}', 'CategoryController@childs');
    });

});
